CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 
 INTRODUCTION
------------

TMGMT LiveWords is a plugin for the Translation Management Tool.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/nitebreed/2632554

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/2632554
   
REQUIREMENTS
------------

This module requires the following modules:

 * Translation Management Tool (https://drupal.org/project/tmgmt)
 
For this plugin to function you have to obtain an user account & api key from
LiveWords (http://www.livewords.com)
 
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------

 * When enabling this module, a default translator plugin is created (see
   Administration » Configuration and modules » Regional & Language
   » Translation Management Translators). Configure this plugin with your
   settings from LiveWords.
   
MAINTAINERS
-----------

Current maintainers:
 * Hans van Wezenbeek (Nitebreed) - https://drupal.org/user/419423
 * Bart van der Hoek (barthje) - https://drupal.org/user/1942906
