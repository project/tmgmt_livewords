<?php

/**
 * @file
 * Provides LiveWords Translator ui controller.
 */

/**
 * LiveWords translator ui controller.
 */
class TMGMTLiveWordsTranslatorUIController extends TMGMTDefaultTranslatorUIController {

  /**
   * {@inheritdoc}
   */
  public function pluginSettingsForm($form, &$form_state, TMGMTTranslator $translator, $busy = FALSE) {
    $succesfull_connection = $this->testConnection($translator);

    if ($succesfull_connection) {
      $form['connection_status'] = array(
        '#prefix' => '<div class="messages status">',
        '#markup' => t('<strong>Connection status:</strong> connection to the server is successful'),
        '#suffix' => '</div>',
      );
    }
    else {
      $form['connection_status'] = array(
        '#prefix' => '<div class="messages warning">',
        '#markup' => t('<strong>Connection status:</strong> connection to the server was not successful, please configure the form correctly.'),
        '#suffix' => '</div>',
      );
    }

    $form['account_domain'] = array(
      '#type' => 'textfield',
      '#title' => t('LiveWords account domain'),
      '#default_value' => $translator->getSetting('account_domain'),
      '#description' => t('Please enter your LiveWords account domain.'),
    );

    $form['api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('LiveWords API key'),
      '#default_value' => $translator->getSetting('api_key'),
      '#description' => t('Please enter your LiveWords API key.'),
    );

    $form['demo'] = array(
      '#type' => 'checkbox',
      '#title' => t('Demo mode'),
      '#default_value' => $translator->getSetting('demo'),
      '#description' => t('Check if you want to use the demo version of the API (api.demo.livewordsflow.com).'),
    );

    // LiveWords needs channels where 'items' of content are sent to. We want
    // the opportunity to map these channels to the Drupal entity types or
    // translation sources.
    $form['channels'] = array(
      '#type' => 'fieldset',
      '#title' => t('1. Channel configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $channels = $translator->getSetting('channels');
    $form['channels']['channels'] = array(
      '#type' => 'textarea',
      '#title' => t('List of channels'),
      '#description' => t('Add the channels as they are configured in LiveWords. Add only one channel per line. Save the form before adding them to the different types.'),
      '#default_value' => !empty($channels['channels']) ? $channels['channels'] : '',
    );

    $form['channel_types'] = array(
      '#type' => 'fieldset',
      '#title' => t('2. Channel to type configuration'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    // Only let the user configure the types if there are channels. Otherwise
    // give a warning.
    if (!empty($channels['channels'])) {
      // All the entities that should not be translatable and thus not be
      // configured for a channel.
      $types_to_ignore = array(
        'redirect',
        'tmgmt_job',
        'tmgmt_job_item',
        'tmgmt_message',
        'tmgmt_translator',
        'tmgmt_remote',
        'rules_config',
        'i18n_translation',
      );

      // First we get all the entities.
      $types = entity_get_info();

      // Now we add the types that are not entities.
      $types['menu'] = array(
        'bundles' => array(
          'menu' => array(
            'label' => 'menu',
          ),
        ),
      );
      $types['strings'] = array(
        'bundles' => array(
          'strings' => array(
            'label' => 'strings',
          ),
        ),
      );

      // Get all the channels and build the option list.
      $channels = explode(PHP_EOL, $channels['channels']);

      $channel_options = array(
        '' => t('- Choose a channel -'),
      );

      foreach ($channels as $channel) {
        $channel = trim($channel);
        $channel_options[$channel] = $channel;
      }

      // Get the settings of all the types.
      $channel_settings = $translator->getSetting('channel_types');

      $form['channel_types']['default_channel'] = array(
        '#type' => 'select',
        '#title' => t('Default channel'),
        '#description' => t('Choose the default value. This channel will be used if a type is not configured.'),
        '#default_value' => !empty($channel_settings['default_channel']) ? $channel_settings['default_channel'] : '',
        '#options' => $channel_options,
      );

      // Loop through all the entity types and bundles to create the form. We
      // skip the entity types that are to be ignored.
      foreach ($types as $type => $entity_info) {
        if (!in_array($type, $types_to_ignore)) {
          $form['channel_types'][$type] = array(
            '#type' => 'fieldset',
            '#title' => $type,
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
          );

          foreach ($entity_info['bundles'] as $key => $bundle) {
            $form['channel_types'][$type][$key] = array(
              '#type' => 'select',
              '#title' => $bundle['label'],
              '#options' => $channel_options,
              '#default_value' => !empty($channel_settings[$type][$key]) ? $channel_settings[$type][$key] : '',
            );
          }
        }
      }
    }
    else {
      $form['channel_types']['no_channels'] = array(
        '#markup' => t('You need to add channels first to configure the different types.'),
      );
    }

    return parent::pluginSettingsForm($form, $form_state, $translator);
  }

  /**
   * Test the connection with the given configuration.
   *
   * @param object $translator
   *   The translator.
   *
   * @return bool
   *   Returns TRUE if a connection was successfully made.
   */
  protected function testConnection($translator) {
    // Get the settings.
    $account_domain = $translator->getSetting('account_domain');
    $api_key = $translator->getSetting('api_key');

    // Get the right URL to use.
    $url = API_URL;

    if ($translator->getSetting('demo')) {
      $url = API_DEMO_URL;
    }

    $request_url = "https://$account_domain:$api_key@$url";
    $options = array(
      'method' => 'GET',
    );

    // Do a request to see if we can successfully connect to the server.
    $result = drupal_http_request($request_url, $options);

    return ($result->code == 200);
  }

}
