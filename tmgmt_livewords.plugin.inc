<?php

/**
 * @file
 * Provides LiveWords Translator plugin controller.
 */

/**
 * LiveWords translator plugin controller.
 */
class TMGMTLiveWordsTranslatorPluginController extends TMGMTDefaultTranslatorPluginController {

  /**
   * {@inheritdoc}
   */
  public function isAvailable(TMGMTTranslator $translator) {
    $channel_types = $translator->getSetting('channel_types');

    // Only allow translations if the API key and domain are configured and if
    // a default channel has been chosen.
    if ($translator->getSetting('api_key') &&
      $translator->getSetting('account_domain') &&
      !empty($channel_types['default_channel'])) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function canTranslate(TMGMTTranslator $translator, TMGMTJob $job) {
    if (!parent::canTranslate($translator, $job)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(TMGMTJob $job) {
    // Make sure each item only exists in this job, not in older jobs!
    $this->removeOldItems($job);

    // Get the base URL for LiveWords.
    $url = $this->getUrl($job);

    // Determine which channels we need to use for this job request.
    $items = $job->getItems();
    $items_per_source = array();
    $items_per_channel = array();

    // First, define which translation sources we have.
    foreach ($items as $tjiid => $item) {
      $items_per_source[$item->item_type][$tjiid] = $item;
    }

    // Then, define which channels are mapped and collect data for each channel.
    foreach ($items_per_source as $item_type => $items) {
      // Check if our source item is an entity.
      $info = entity_get_info($item_type);

      if (isset($info)) {
        // For entities, channels can vary per bundle.
        // So make sure to check the bundle of each item.
        foreach ($items as $key => $item) {
          if ($item->plugin == 'i18n_string') {
            list(, , $entity_id) = explode(':', $item->item_id, 3);
          }
          else {
            $entity_id = $item->item_id;
          }

          $wrapper = entity_metadata_wrapper($item_type, $entity_id);
          $bundle = $wrapper->getBundle();

          // Get the channel.
          $channel = $this->getChannel($job, $item_type, $bundle);

          // Assemble our array.
          $items_per_channel[$channel][$key] = $item;
        }
      }
      else {
        // Not all translation sources are entities and therefor don't have a
        // bundle. In that case the bundle is the same as the sources (see
        // the configuration form).
        $bundle = $item_type;

        // Get the channel.
        $channel = $this->getChannel($job, $item_type, $bundle);

        // Assemble our array.
        if (isset($items_per_channel[$channel])) {
          $items_per_channel[$channel] += $items;
        }
        else {
          $items_per_channel[$channel] = $items;
        }
      }
    }

    // Define an array where we keep track of request errors.
    $errors = array();

    // Collect the XML for each channel and send it to LiveWords.
    foreach ($items_per_channel as $channel => $items) {
      // Get the XML we will send to LiveWords.
      $xml = $this->formatXml($job, $items);

      $options = array(
        'method' => 'POST',
        'data' => $xml,
        'headers' => array(
          'Content-Type' => 'text/xml',
        ),
      );

      $query = array(
        'target-language' => $job->target_language,
        'reset-all-target-languages' => 'false',
      );

      $result = drupal_http_request("$url/$channel/items/_bulk?" . drupal_http_build_query($query), $options);

      if ($result->code != 200) {
        $errors[$result->status_message] = $result->status_message;
      }
    }

    if (empty($errors)) {
      // The translation job has been successfully submitted.
      $job->submitted('The translation job has been submitted to LiveWords.');
    }
    else {
      // The translation job has not been successfully submitted.
      $job->rejected('The translation job has not been submitted to LiveWords: @errors', array('@errors' => implode('<br/>', $errors)));
    }
  }

  /**
   * Process the translation to add to the job.
   *
   * @param SimpleXMLElement $xml_item
   *   The XML item to process.
   * @param TMGMTJob $job
   *   The translation job.
   * @param TMGMTJobItem $job_item
   *   The translation job item.
   */
  public function processTranslation(SimpleXMLElement $xml_item, TMGMTJob $job, TMGMTJobItem $job_item) {
    $translation = array();
    $properties = $xml_item->xpath('property');

    foreach ($properties as $property) {
      $key = (string) $property->attributes()->id;
      $translation[$key]['#text'] = (string) $property->value;
    }

    $job->addTranslatedData(tmgmt_unflatten_data($translation));
  }

  /**
   * Process the callback received from LiveWords.
   *
   * It finds all the nodes in the XML, finds the corresponding job item and
   * sends the node to the translation processor.
   *
   * @param SimpleXMLElement $xml
   *   The XML item to process.
   *
   * @return bool
   *   Returns TRUE if the translations were successful.
   */
  public function processCallback(SimpleXMLElement $xml) {

    return FALSE;
  }

  /**
   * Format XML for LiveWords.
   *
   * @param TMGMTJob $job
   *   The translation job.
   * @param array $job_items
   *   An array with job items.
   *
   * @return string
   *   The formatted XML.
   */
  public function formatXml(TMGMTJob $job, array $job_items) {
    $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><nodes/>');

    foreach ($job_items as $tjiid => $job_item) {
      $xml_node = $xml->addChild('node');
      $xml_node->addAttribute('item_type', $job_item->item_type);
      $xml_node->addAttribute('id', $job_item->item_id);

      // Send the title of the node, or if the item isn't a node, send a string
      // containing information about the item.
      if (isset($job_item->data['node_title']['#text'])) {
        $xml_node->addAttribute('name', $job_item->data['node_title']['#text']);
      }
      else {
        $xml_node->addAttribute('name', $job_item->plugin . ':' . $job_item->item_id);
      }
      // Pull the source data array through the job and flatten it.
      $data = array_filter(tmgmt_flatten_data($job->getData(array($tjiid))), '_tmgmt_filter_data');

      foreach ($data as $key => $value) {
        $property = $xml_node->addChild('property');
        $property->addAttribute('id', $key);
        $property->addAttribute('type', 'String');
        $property->addAttribute('label', $value['#label']);
        // Add our string (which can contain HTML) as CDATA, so it won't be
        // interpreted as XML markup.
        $this->addCdata('value', $value['#text'], $property);
      }
    }

    return $xml->asXML();
  }

  /**
   * Adds a CDATA property to an XML document.
   *
   * @param string $name
   *   Name of property that should contain CDATA.
   * @param string $value
   *   Value that should be inserted into a CDATA child.
   * @param object $parent
   *   Element that the CDATA child should be attached too.
   */
  private function addCdata($name, $value, &$parent) {
    $child = $parent->addChild($name);

    if ($child !== NULL) {
      $child_node = dom_import_simplexml($child);
      $child_owner = $child_node->ownerDocument;
      $child_node->appendChild($child_owner->createCDATASection($value));
    }

    return $child;
  }

  /**
   * Get the channel based on the type of translation item.
   *
   * The type could be an entity type, or menu item or just a normal string.
   *
   * @param TMGMTJob $job
   *   The job of the translation. Used to get the translator settings.
   * @param string $type
   *   The type of the item that is being translated. For example, node.
   * @param string $bundle
   *   The bundle of the item that is being translated. For example, page.
   *
   * @return string
   *   The channel to use for the translation item.
   */
  public function getChannel(TMGMTJob $job, $type, $bundle) {
    $channel_settings = $this->getTranslatorSetting($job, 'channel_types');

    // Check if a channel is set for this type and bundle. If there is no
    // channel set we return the default channel.
    if (!empty($channel_settings[$type][$bundle])) {
      return $channel_settings[$type][$bundle];
    }
    else {
      return $channel_settings['default_channel'];
    }
  }

  /**
   * Get the URL of the LiveWords REST API.
   *
   * @param TMGMTJob $job
   *   The job of the translation. Used to get the translator settings.
   *
   * @return string
   *   The url of the LiveWords REST API.
   */
  public function getUrl(TMGMTJob $job) {
    // Get the API settings.
    $account_domain = $this->getTranslatorSetting($job, 'account_domain');
    $api_key = $this->getTranslatorSetting($job, 'api_key');

    // Get the right URL to use.
    $url = API_URL;

    if ($this->getTranslatorSetting($job, 'demo')) {
      $url = API_DEMO_URL;
    }

    return "https://$account_domain:$api_key@$url";
  }

  /**
   * Helper function to get a setting from the translator.
   *
   * @param TMGMTJob $job
   *   The job of the translation. Used to get the translator settings.
   * @param string $setting
   *   The setting you want to get.
   *
   * @return mixed
   *   The settings as defined in the translator.
   */
  public function getTranslatorSetting(TMGMTJob $job, $setting) {
    static $translator;

    if (empty($translator)) {
      $translator = $job->getTranslator();
    }

    return $translator->getSetting($setting);
  }

  /**
   * Remove items from older jobs.
   *
   * We only want an item to be in one job.
   *
   * @param TMGMTJob $job
   *   The new job.
   */
  public function removeOldItems(TMGMTJob $job) {
    $items = $job->getItems();

    // Loop through all the items of the job to find the item in an older job.
    foreach ($items as $item) {
      $query = db_select('tmgmt_job_item', 'item')
        ->fields('item', array('tjiid'))
        ->condition('item.item_id', $item->item_id)
        ->condition('item.item_type', $item->item_type)
        ->condition('item.tjid', $job->tjid, '<>');

      // Join with the job so we only remove jobs with the same source and
      // target language.
      $query->leftJoin('tmgmt_job', 'job', 'job.tjid = item.tjid');
      $query->condition('job.source_language', $job->source_language);
      $query->condition('job.target_language', $job->target_language);

      $result = $query->execute();
      // Loop through the items and delete them.
      while ($row = $result->fetchField()) {
        // Get the old item and its job.
        $old_item = tmgmt_job_item_load($row);
        $old_job = $old_item->getJob();

        // Remove the old item.
        $old_item->delete();

        // Get the items of the old job and delete the job if there are no items
        // left.
        $old_job_items = $old_job->getItems();
        if (empty($old_job_items)) {
          $old_job->delete();
        }
      }
    }
  }

}
